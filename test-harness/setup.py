#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

with open('README.md') as readme_file:
    readme = readme_file.read()
#moved test_depndencies to a seperate file so that they can be loaded with pip install during development
with open('test_requirements.txt') as f:
    test_requirements = f.read().splitlines()

setup(
    name='skampi',
    version='0.0.0',
    description="",
    long_description=readme + '\n\n',
    author="Matteo Di Carlo System Team",
    author_email='matteo.dicarlo@inaf.it',
    url='https://github.com/ska-telescope/tango-example',
    packages=[ ],
    include_package_data=True,
    license="BSD license",
    zip_safe=False,
    keywords='skampi',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    test_suite='tests',
    install_requires=['pytango'],  # FIXME: add your package's dependencies to this list
    setup_requires=[
        # dependency for `python setup.py test`
        'pytest-runner',
        # dependencies for `python setup.py build_sphinx`
        'sphinx',
        'recommonmark'
    ],
    tests_require=test_requirements,
    extras_require={
        'dev':  ['prospector[with_pyroma]', 'yapf', 'isort']
    }
)
